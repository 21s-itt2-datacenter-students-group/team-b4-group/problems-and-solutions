# List of potentional vulnerabilities(security)

* The mqtt server and dashboard can be accessible by other users.

*  Description of the consequence- other users could read, steal data from the server or dashboard.

# Solution methods: 

- Secure SSH between client and server  (by creating and setting up SSH keys)

- Make a "private" broker instead of using public brokers, that can only be acces by certain computers (firewall?).

- Use a encrypted MQTT traffic method.

- Secure NodeRed with user login and password (OME (read only) and IT-students (read/write privileges))


